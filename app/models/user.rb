class User
  include Mongoid::Document
  has_one :employee
  has_one :role
end
