class Classroom
  include Mongoid::Document
  field :grade, type: Integer
  field :room, type: Integer
  has_many :teachers
  has_many :students
end
