class Student < Person
  belongs_to :classroom
  has_and_belongs_to_many :parents
end
